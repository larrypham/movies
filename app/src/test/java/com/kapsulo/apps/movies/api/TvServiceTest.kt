package com.kapsulo.apps.movies.api

import com.kapsulo.apps.movies.LiveDataTestUtil
import org.hamcrest.CoreMatchers.`is`
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import java.io.IOException

class TvServiceTest : ApiAbstract<TvService>() {
	
	private lateinit var service: TvService
	
	@Before
	fun initService() {
		this.service = createService(TvService::class.java)
	}
	
	@Throws(IOException::class)
	@Test
	fun fetchTvKeywordsTest() {
		enqueueResponse("/tmdb_movie_keywords.json")
		
		val response = LiveDataTestUtil.getValue(service.fetchKeywords(1))
		Assert.assertThat(response.body?.id, `is`(550))
		Assert.assertThat(response.body?.keywords?.get(0)?.id, `is`(825))
		Assert.assertThat(response.body?.keywords?.get(0)?.name, `is`("support group"))
	}
	
	@Throws(IOException::class)
	@Test
	fun fetchTvVideoTest() {
		enqueueResponse("/tmdb_movie_videos.json")
		
		val response = LiveDataTestUtil.getValue(service.fetchVideos(1))
		Assert.assertThat(response.body?.id, `is`(550))
		Assert.assertThat(response.body?.videos?.get(0)?.id, `is`("533ec654c3a36854480003eb"))
		Assert.assertThat(response.body?.videos?.get(0)?.iso6391, `is`("en"))
		Assert.assertThat(response.body?.videos?.get(0)?.iso31661, `is`("US"))
		Assert.assertThat(response.body?.videos?.get(0)?.key, `is`("SUXWAEX2jlg"))
	}
	
	@Throws(IOException::class)
	@Test
	fun fetchReviewsTest() {
		enqueueResponse("/tmdb_movie_reviews.json")
		
		val response = LiveDataTestUtil.getValue(service.fetchReviews(1))
		Assert.assertThat(response.body?.id, `is`(297761))
		Assert.assertThat(response.body?.reviews?.get(0)?.id, `is`("57a814dc9251415cfb00309a"))
		Assert.assertThat(response.body?.reviews?.get(0)?.author, `is`("Frank Ochieng"))
		Assert.assertThat(response.body?.reviews?.get(0)?.url, `is`("https://www.themoviedb.org/review/57a814dc9251415cfb00309a"))
		
		Assert.assertThat(response.body?.totalPages, `is`(1))
		Assert.assertThat(response.body?.totalResults, `is`(1))
	}
}