package com.kapsulo.apps.movies.repository

import android.arch.core.executor.testing.InstantTaskExecutorRule
import android.arch.lifecycle.Observer
import com.kapsulo.apps.movies.MockTestUtil
import com.kapsulo.apps.movies.api.ApiUtil.successCall
import com.kapsulo.apps.movies.api.MovieService
import com.kapsulo.apps.movies.models.Keyword
import com.kapsulo.apps.movies.models.Resource
import com.kapsulo.apps.movies.models.Review
import com.kapsulo.apps.movies.models.Video
import com.kapsulo.apps.movies.models.networks.KeywordListResponse
import com.kapsulo.apps.movies.models.networks.ReviewListResponse
import com.kapsulo.apps.movies.models.networks.VideoListResponse
import com.kapsulo.apps.movies.room.MovieDao
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.verifyNoMoreInteractions
import com.nhaarman.mockitokotlin2.whenever
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4

@RunWith(JUnit4::class)
class MovieRepositoryTest {
	
	private lateinit var repository: MovieRepository
	
	private val movieDao = mock<MovieDao>()
	private val service = mock<MovieService>()
	
	@Rule
	@JvmField
	val instantExecutorRule = InstantTaskExecutorRule()
	
	@Before
	fun init() {
		repository = MovieRepository(service, movieDao)
	}

	@Test
	fun loadKeywordListFromNetwork() {
		val loadFromDB = MockTestUtil.mockMovie()
		whenever(movieDao.getMovie(123)).thenReturn(loadFromDB)
		
		// Asserts an mocking keywords-response, assuming the calling of apis from service is success, verify the flow.
		val mockResponse = KeywordListResponse(123, MockTestUtil.mockKeywordList())
		val call = successCall(mockResponse)
		whenever(service.fetchKeywords(123)).thenReturn(call)
		
		val data = repository.loadKeywordList(123)
		verify(movieDao).getMovie(123)
		verifyNoMoreInteractions(service)
		
		val observer = mock<Observer<Resource<List<Keyword>>>>()
		data.observeForever(observer)
		verify(observer).onChanged(Resource.success(MockTestUtil.mockKeywordList(), true))
	}
	
	@Test
	fun loadVideoListFromNetwork() {
		val loadFromDB = MockTestUtil.mockMovie()
		whenever(movieDao.getMovie(123)).thenReturn(loadFromDB)
		
		// Asserts a mocking videos-response, assume the call of apis from service is success, verify the flow.
		val mockResponse = VideoListResponse(123, MockTestUtil.mockVideoList())
		val call = successCall(mockResponse)
		whenever(service.fetchVideos(123)).thenReturn(call)
		
		val data = repository.loadVideoList(123)
		verify(movieDao).getMovie(123)
		verifyNoMoreInteractions(service)
		
		val observer = mock<Observer<Resource<List<Video>>>>()
		data.observeForever(observer)
		verify(observer).onChanged(Resource.success(MockTestUtil.mockVideoList(), true))
	}
	
	@Test
	fun loadReviewListFromNetwork() {
		val loadFromDB = MockTestUtil.mockMovie()
		whenever(movieDao.getMovie(123)).thenReturn(loadFromDB)
		
		val mockResponse = ReviewListResponse(123, 1, MockTestUtil.mockReviewList(), 100, 100)
		val call = successCall(mockResponse)
		whenever(service.fetchReviews(123)).thenReturn(call)
		
		val data = repository.loadReviewsList(123)
		verify(movieDao).getMovie(123)
		verifyNoMoreInteractions(service)
		
		val observer = mock<Observer<Resource<List<Review>>>>()
		data.observeForever(observer)
		verify(observer).onChanged(Resource.success(MockTestUtil.mockReviewList(), true))
		
		val updateMovie = MockTestUtil.mockMovie()
		updateMovie.reviews = MockTestUtil.mockReviewList()
		verify(movieDao).updateMovie(updateMovie)
	}
}