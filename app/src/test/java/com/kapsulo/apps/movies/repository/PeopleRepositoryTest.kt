package com.kapsulo.apps.movies.repository

import android.arch.core.executor.testing.InstantTaskExecutorRule
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.Observer
import com.kapsulo.apps.movies.MockTestUtil
import com.kapsulo.apps.movies.api.ApiUtil.successCall
import com.kapsulo.apps.movies.api.PeopleService
import com.kapsulo.apps.movies.models.Resource
import com.kapsulo.apps.movies.models.entity.Person
import com.kapsulo.apps.movies.models.networks.PeopleResponse
import com.kapsulo.apps.movies.models.networks.PersonDetail
import com.kapsulo.apps.movies.room.PeopleDao
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.verifyNoMoreInteractions
import com.nhaarman.mockitokotlin2.whenever
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4

@RunWith(JUnit4::class)
class PeopleRepositoryTest {
	private lateinit var repository: PeopleRepository
	private val peopleDao = mock<PeopleDao>()
	private val service = mock<PeopleService>()
	
	@Rule
	@JvmField
	val instantExectorRule = InstantTaskExecutorRule()
	
	@Before
	fun init() {
		repository = PeopleRepository(service, peopleDao)
	}
	
	@Test
	fun loadPeopleFromNetwork() {
		val loadFromDB = MutableLiveData<List<Person>>()
		whenever(peopleDao.getPeople(1)).thenReturn(loadFromDB)
		
		val mockResponse = PeopleResponse(1, emptyList(), 100, 10)
		val call = successCall(mockResponse)
		whenever(service.fetchPopularPeople(1)).thenReturn(call)
		
		val data = repository.loadPeople(1)
		verify(peopleDao).getPeople(1)
		verifyNoMoreInteractions(peopleDao)
		
		val observer = mock<Observer<Resource<List<Person>>>>()
		data.observeForever(observer)
		verifyNoMoreInteractions(service)
		
		val updateData = MutableLiveData<List<Person>>()
		whenever(peopleDao.getPeople(1)).thenReturn(updateData)
		
		loadFromDB.postValue(null)
		verify(observer).onChanged(Resource.loading(null))
		verify(service).fetchPopularPeople(1)
		verify(peopleDao).insertPeople(mockResponse.results)
		
		updateData.postValue(mockResponse.results)
		verify(observer).onChanged(Resource.success(mockResponse.results, false))
	}
	
	@Test
	fun loadPersonDetailFromNetwork() {
		val loadFromDB = MockTestUtil.mockPerson()
		whenever(peopleDao.getPerson(123)).thenReturn(loadFromDB)
		
		val mockResponse = MockTestUtil.mockPersonDetail()
		val call = successCall(mockResponse)
		whenever(service.fetchPersonDetail(123)).thenReturn(call)
		
		val data = repository.loadPersonDetail(123)
		verify(peopleDao).getPerson(123)
		verifyNoMoreInteractions(service)
		
		val observer = mock<Observer<Resource<PersonDetail>>>()
		data.observeForever(observer)
		verify(observer).onChanged(Resource.success(MockTestUtil.mockPersonDetail(), true))
		
		val updatedPerson = MockTestUtil.mockPerson()
		updatedPerson.personDetail = MockTestUtil.mockPersonDetail()
		verify(peopleDao).updatePerson(updatedPerson)
	}
}