package com.kapsulo.apps.movies.api

import org.hamcrest.CoreMatchers
import org.hamcrest.CoreMatchers.`is`
import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import retrofit2.Response

@RunWith(JUnit4::class)
class ApiResponseTest {
	
	@Test
	fun exception() {
		val exception = Exception("foo")
		val apiResponse = ApiResponse<String>(exception)

		// Asserts the response's success is false.
		Assert.assertThat(apiResponse.isSuccessful, `is`(false))
		
		// Asserts the response's body is null-value.
		Assert.assertThat<String>(apiResponse.body, CoreMatchers.nullValue())
		
		// Asserts the response's code is 500
		Assert.assertThat(apiResponse.code, `is`(500))
		
		// Asserts the response's message is foo
		Assert.assertThat(apiResponse.message, `is`("foo"))
	}
	
	@Test
	fun success() {
		val apiResponse = ApiResponse(Response.success("foo"))
		
		// Asserts the response's success is true
		Assert.assertThat(apiResponse.isSuccessful, `is`(true))
		
		// Asserts the response's code is 200
		Assert.assertThat(apiResponse.code, `is`(200))
		
		// Asserts the response's body is "foo"
		Assert.assertThat<String>(apiResponse.body, `is`("foo"))
		
		// Asserts the response's message is null.
		Assert.assertThat(apiResponse.message, CoreMatchers.nullValue())
	}
}