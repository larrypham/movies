package com.kapsulo.apps.movies.api

import com.kapsulo.apps.movies.LiveDataTestUtil
import org.hamcrest.CoreMatchers.`is`
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import java.io.IOException

class TheDiscoverServiceTest : ApiAbstract<TheDiscoverService>() {
	
	private lateinit var service: TheDiscoverService
	
	@Before
	fun initService() {
		this.service = createService(TheDiscoverService::class.java)
	}
	
	@Throws(IOException::class)
	@Test
	fun fetchMovieListTest() {
		enqueueResponse("/tmdb_movie.json")
		val response = LiveDataTestUtil.getValue(service.fetchDiscoverMovie(1))
		
		Assert.assertThat(response.body?.results?.get(0)?.id, `is`(283552))
		Assert.assertThat(response.body?.totalResults, `is`(222))
		Assert.assertThat(response.body?.totalPages, `is`(12))
	}
	
	@Throws(IOException::class)
	@Test
	fun fetchTvListTest() {
		enqueueResponse("/tmdb_tv.json")
		val response = LiveDataTestUtil.getValue(service.fetchDiscoverTv(1))
		
		Assert.assertThat(response.body?.results?.get(0)?.id, `is`(61889))
		Assert.assertThat(response.body?.totalPages, `is`(3074))
		Assert.assertThat(response.body?.totalResults, `is`(61470))
	}
}