package com.kapsulo.apps.movies.api

import com.kapsulo.apps.movies.LiveDataTestUtil
import org.hamcrest.CoreMatchers.`is`
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import java.io.IOException

class PeopleServiceTest : ApiAbstract<PeopleService>() {
	
	private lateinit var service: PeopleService
	
	@Before
	fun initService() {
		this.service = createService(PeopleService::class.java)
	}
	
	@Throws(IOException::class)
	@Test
	fun fetchPersonListTest() {
		// Enqueueing the simulated result-json file to the mocking webserver
		enqueueResponse("/tmdb_people.json")
		
		// Getting the mocking result from file.
		val response = LiveDataTestUtil.getValue(service.fetchPopularPeople(1))
		
		// Check the id of first person object in this list value with expected result.
		Assert.assertThat(response.body?.results?.get(0)?.id, `is`(28782))
		
		// Asserts the total pages of json's body with the expected value.
		Assert.assertThat(response.body?.totalPages, `is`(984))
		
		// Asserts the total results of json's body with the expected value.
		Assert.assertThat(response.body?.totalResults, `is`(19671))
	}
	
	@Throws(IOException::class)
	@Test
	fun fetchPersonDetail() {
		// Enqueueing the simulated result-json file to the mocking webserver.
		enqueueResponse("/tmdb_person.json")
		
		// Getting the mocking result from file.
		val response = LiveDataTestUtil.getValue(service.fetchPersonDetail(123))
		
		// Asserts the fields of the response's body with the expected values.
		Assert.assertThat(response.body?.birthDay, `is`("1963-12-18"))
		Assert.assertThat(response.body?.knownForDepartment, `is`("Acting"))
		Assert.assertThat(response.body?.placeOfBirth, `is`("Shawnee, Oklahoma, USA"))
	}
}