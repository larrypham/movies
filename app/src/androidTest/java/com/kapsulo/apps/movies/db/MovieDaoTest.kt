package com.kapsulo.apps.movies.db

import android.support.test.runner.AndroidJUnit4
import com.kapsulo.apps.movies.LiveDataTestUtil
import com.kapsulo.apps.movies.MockTestUtil
import com.kapsulo.apps.movies.models.entity.Movie
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class MovieDaoTest: DBTest() {
	
	@Test
	fun insertAndReadTest() {
		val movieList = ArrayList<Movie>()
		val movie = MockTestUtil.mockMovie()
		movieList.add(movie)
		
		db.movieDao().insertMovieList(movieList)
		val loadedFromDB = LiveDataTestUtil.getValue(db.movieDao().getMovieList(movie.page))[0]
		assertThat(loadedFromDB.page, `is`(1))
		assertThat(loadedFromDB.id, `is`(123))
	}
	
	@Test
	fun updateAndReadTest() {
		val movieList = ArrayList<Movie>()
		val movie = MockTestUtil.mockMovie()
		movieList.add(movie)
		db.movieDao().insertMovieList(movieList)
		
		val loadedFromDB = db.movieDao().getMovie(movie.id)
		assertThat(loadedFromDB.page, `is`(1))
		
		movie.page = 10
		db.movieDao().updateMovie(movie)
		
		val updated = db.movieDao().getMovie(movie.id)
		assertThat(updated.page, `is`(10))
	}
}