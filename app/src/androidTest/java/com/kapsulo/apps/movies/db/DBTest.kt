package com.kapsulo.apps.movies.db

import android.arch.persistence.room.Room
import android.support.test.InstrumentationRegistry
import android.support.test.runner.AndroidJUnit4
import com.kapsulo.apps.movies.room.AppDatabase
import org.junit.After
import org.junit.Before
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
abstract class DBTest {
	lateinit var db: AppDatabase
	
	@Before
	fun initDB() {
		db = Room.inMemoryDatabaseBuilder(InstrumentationRegistry.getContext(), AppDatabase::class.java).build()
	}
	
	@After
	fun closeDB() {
		db.close()
	}
}