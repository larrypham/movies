package com.kapsulo.apps.movies.db

import android.support.test.runner.AndroidJUnit4
import com.kapsulo.apps.movies.LiveDataTestUtil
import com.kapsulo.apps.movies.MockTestUtil
import com.kapsulo.apps.movies.models.entity.Person
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class PeopleDaoTest: DBTest() {
	
	@Test
	fun insertAndReadTest() {
		val people = ArrayList<Person>()
		val mockPerson = MockTestUtil.mockPerson()
		people.add(mockPerson)
		
		db.peopleDao().insertPeople(people)
		val loadedFromDB = LiveDataTestUtil.getValue(db.peopleDao().getPeople(1))[0]
		assertThat(loadedFromDB.page, `is`(1))
		assertThat(loadedFromDB.id, `is`(123))
	}
	
	@Test
	fun updateAndRead() {
		val people = ArrayList<Person>()
		val mockPerson = MockTestUtil.mockPerson()
		people.add(mockPerson)
		
		db.peopleDao().insertPeople(people)
		
		val loadedFromDB = db.peopleDao().getPerson(mockPerson.id)
		assertThat(loadedFromDB.page, `is`(1))
		
		loadedFromDB.page = 10
		db.peopleDao().updatePerson(loadedFromDB)
		
		val updated = db.peopleDao().getPerson(mockPerson.id)
		assertThat(updated.page, `is`(10))
	}
}