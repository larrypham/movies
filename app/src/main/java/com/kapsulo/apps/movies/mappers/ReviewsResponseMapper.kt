package com.kapsulo.apps.movies.mappers

import com.kapsulo.apps.movies.models.networks.ReviewListResponse

class ReviewsResponseMapper : NetworkResponseMapper<ReviewListResponse> {
	override fun onLastPage(response: ReviewListResponse): Boolean {
		return true
	}
}
