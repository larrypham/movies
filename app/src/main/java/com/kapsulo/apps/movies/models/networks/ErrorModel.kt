package com.kapsulo.apps.movies.models.networks

data class ErrorModel(val statusCode: Int, val statusMessage: String, val success: Boolean)