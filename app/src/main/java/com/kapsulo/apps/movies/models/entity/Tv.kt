package com.kapsulo.apps.movies.models.entity

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import com.kapsulo.apps.movies.models.Keyword
import com.kapsulo.apps.movies.models.Review
import com.kapsulo.apps.movies.models.Video

@Entity(tableName = "Tv")
data class Tv(
		var page: Int,
		var keywords: List<Keyword>? = ArrayList(),
		var videos: List<Video>? = ArrayList(),
		var reviews: List<Review>? = ArrayList(),
		
		@SerializedName("poster_path")
		val posterPath: String?,
		
		@SerializedName("popularity")
		val popularity: Float,
		
		@PrimaryKey
		@SerializedName("id")
		val id: Int,
		
		@SerializedName("backdrop_path")
		val backdropPath: String?,
		
		@SerializedName("vote_average")
		val voteAverage: Float,
		
		@SerializedName("overview")
		val overview: String?,
		
		@SerializedName("first_air_date")
		val firstAirDate: String?,
		
		@SerializedName("origin_country")
		val originCountry: List<String>?,
		
		@SerializedName("genre_ids")
		val genreIds: List<Int>,
		
		@SerializedName("original_language")
		val originalLanguage: String?,
		
		@SerializedName("vote_count")
		val voteCount: Int,
		
		@SerializedName("name")
		val name: String?,
		
		@SerializedName("original_name")
		val originalName: String?
) : Parcelable {
	constructor(parcel: Parcel) : this(
			parcel.readInt(),
			ArrayList<Keyword>().apply { parcel.readList(this, Keyword::class.java.classLoader) },
			ArrayList<Video>().apply { parcel.readList(this, Video::class.java.classLoader) },
			ArrayList<Review>().apply { parcel.readList(this, Review::class.java.classLoader) },
			parcel.readString(),
			parcel.readFloat(),
			parcel.readInt(),
			parcel.readString(),
			parcel.readFloat(),
			parcel.readString(),
			parcel.readString(),
			parcel.createStringArrayList(),
			ArrayList<Int>().apply { parcel.readList(this, Int::class.java.classLoader) },
			parcel.readString(),
			parcel.readInt(),
			parcel.readString(),
			parcel.readString())
	
	override fun writeToParcel(dest: Parcel?, flags: Int) = with(dest!!) {
		writeInt(page)
		writeList(keywords)
		writeList(videos)
		writeList(reviews)
		writeString(posterPath)
		writeFloat(popularity)
		
		writeInt(id)
		writeString(backdropPath)
		writeFloat(voteAverage)
		writeString(overview)
		writeString(firstAirDate)
		writeStringList(originCountry)
		writeList(genreIds)
		writeString(originalLanguage)
		
		writeInt(voteCount)
		writeString(name)
		writeString(originalName)
	}
	
	override fun describeContents(): Int = 0
	
	companion object CREATOR : Parcelable.Creator<Tv> {
		override fun createFromParcel(parcel: Parcel): Tv {
			return Tv(parcel)
		}
		
		override fun newArray(size: Int): Array<Tv?> {
			return arrayOfNulls(size)
		}
	}
}