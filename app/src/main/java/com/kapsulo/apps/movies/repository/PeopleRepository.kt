package com.kapsulo.apps.movies.repository

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import com.kapsulo.apps.movies.api.ApiResponse
import com.kapsulo.apps.movies.api.PeopleService
import com.kapsulo.apps.movies.mappers.PeopleResponseMapper
import com.kapsulo.apps.movies.mappers.PersonDetailResponseMapper
import com.kapsulo.apps.movies.models.Resource
import com.kapsulo.apps.movies.models.entity.Person
import com.kapsulo.apps.movies.models.networks.PeopleResponse
import com.kapsulo.apps.movies.models.networks.PersonDetail
import com.kapsulo.apps.movies.room.PeopleDao
import timber.log.Timber
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class PeopleRepository @Inject
constructor(val service: PeopleService, val peopleDao: PeopleDao): Repository {
	
	fun loadPeople(page: Int): LiveData<Resource<List<Person>>> {
		return object : NetworkBoundRepository<List<Person>, PeopleResponse, PeopleResponseMapper>() {
			override fun saveFetchData(items: PeopleResponse) {
				items.results.forEach { item -> item.page = page }
				peopleDao.insertPeople(people = items.results)
			}
			
			override fun shouldFetch(data: List<Person>?): Boolean {
				return data == null || data.isEmpty()
			}
			
			override fun loadFromDB(): LiveData<List<Person>> {
				return peopleDao.getPeople(page_ = page)
			}
			
			override fun fetchService(): LiveData<ApiResponse<PeopleResponse>> {
				return service.fetchPopularPeople(page = page)
			}
			
			override fun mapper(): PeopleResponseMapper {
				return PeopleResponseMapper()
			}
			
			override fun onFetchFailed(message: String?) {
				Timber.d("onFetchFailed: $message")
			}
		}.asLiveData()
	}
	
	fun loadPersonDetail(id: Int): LiveData<Resource<PersonDetail>> {
		return object : NetworkBoundRepository<PersonDetail, PersonDetail, PersonDetailResponseMapper>() {
			override fun saveFetchData(items: PersonDetail) {
				val person = peopleDao.getPerson(id_ = id)
				person.personDetail = items
				peopleDao.updatePerson(person = person)
			}
			
			override fun shouldFetch(data: PersonDetail?): Boolean {
				return data == null || data.biography!!.isEmpty()
			}
			
			override fun loadFromDB(): LiveData<PersonDetail> {
				val person = peopleDao.getPerson(id_ = id)
				val data: MutableLiveData<PersonDetail> = MutableLiveData()
				data.value = person.personDetail
				return data
			}
			
			override fun fetchService(): LiveData<ApiResponse<PersonDetail>> {
				return service.fetchPersonDetail(id = id)
			}
			
			override fun mapper(): PersonDetailResponseMapper {
				return PersonDetailResponseMapper()
			}
			
			override fun onFetchFailed(message: String?) {
				Timber.d("onFetchFailed: $message")
			}
			
		}.asLiveData()
	}
}
