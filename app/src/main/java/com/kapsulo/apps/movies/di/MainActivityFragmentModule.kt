package com.kapsulo.apps.movies.di

import com.kapsulo.apps.movies.view.ui.main.MovieListFragment
import com.kapsulo.apps.movies.view.ui.main.PersonListFragment
import com.kapsulo.apps.movies.view.ui.main.TvListFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Suppress("unused")
@Module
abstract class MainActivityFragmentModule {
	
	@ContributesAndroidInjector
	abstract fun contributeMovieListFragment(): MovieListFragment
	
	@ContributesAndroidInjector
	abstract fun contributeTvListFragment(): TvListFragment
	
	@ContributesAndroidInjector
	abstract fun contributePersonListFragment(): PersonListFragment
}