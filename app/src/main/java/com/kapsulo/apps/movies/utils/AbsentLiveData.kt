package com.kapsulo.apps.movies.utils

import android.arch.lifecycle.LiveData

class AbsentLiveData<T> : LiveData<T>() {
	init {
		postValue(null)
	}
	
	companion object {
		fun <T> create() = AbsentLiveData<T>()
	}
}
