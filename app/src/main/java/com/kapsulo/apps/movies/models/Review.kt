package com.kapsulo.apps.movies.models

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

data class Review(
		@SerializedName("id")
		val id: String,
		
		@SerializedName("author")
		val author: String,
		
		@SerializedName("content")
		val content: String,
		
		@SerializedName("url")
		val url: String
) : Parcelable {
	constructor(parcel: Parcel) : this(
			parcel.readString()!!,
			parcel.readString()!!,
			parcel.readString()!!,
			parcel.readString()!!)
	
	override fun writeToParcel(dest: Parcel?, flags: Int) = with(dest!!) {
		writeString(id)
		writeString(author)
		writeString(content)
		writeString(url)
	}
	
	override fun describeContents(): Int = 0
	
	companion object CREATOR : Parcelable.Creator<Review> {
		override fun createFromParcel(parcel: Parcel): Review {
			return Review(parcel)
		}
		
		override fun newArray(size: Int): Array<Review?> {
			return arrayOfNulls(size)
		}
	}
}
