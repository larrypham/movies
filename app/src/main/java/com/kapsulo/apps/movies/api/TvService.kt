package com.kapsulo.apps.movies.api

import android.arch.lifecycle.LiveData
import com.kapsulo.apps.movies.models.networks.KeywordListResponse
import com.kapsulo.apps.movies.models.networks.ReviewListResponse
import com.kapsulo.apps.movies.models.networks.VideoListResponse
import retrofit2.http.GET
import retrofit2.http.Path

interface TvService {
	
	/**
	 * [Tv Video](https://developers.themoviedb.org/3/tv/get-tv-keywords)
	 *
	 * Get the keywords that have been added to a TV show.
	 *
	 * @param [id] Specify the id of tv keywords.
	 * @return [KeywordListResponse] response.
	 */
	@GET("/3/tv/{tv_id}")
	fun fetchKeywords(@Path("tv_id") id: Int): LiveData<ApiResponse<KeywordListResponse>>
	
	/**
	 * [Tv Videos](https://developers.themoviedb.org/3/tv/get-tv-videos)
	 *
	 * Get the videos that have been added to a TV show.
	 *
	 * @param [id] Specify the id of tv id.
	 * @return [VideoListResponse] response.
	 */
	@GET("/3/tv/{tv_id}/videos")
	fun fetchVideos(@Path("tv_id") id: Int): LiveData<ApiResponse<VideoListResponse>>
	
	/**
	 * [Tv Reviews](https://developers.themoviedb.org/3/tv/get-tv-reviews)
	 *
	 * Get the reviews that have been added to a TV show.
	 *
	 * @param [id] Specify the id of tv id.
	 * @return [ReviewListResponse]
	 */
	@GET("/3/tv/{tv_id}/reviews")
	fun fetchReviews(@Path("tv_id") id: Int): LiveData<ApiResponse<ReviewListResponse>>
}