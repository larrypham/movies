package com.kapsulo.apps.movies.room

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import android.arch.persistence.room.TypeConverters
import com.kapsulo.apps.movies.models.entity.Movie
import com.kapsulo.apps.movies.models.entity.Person
import com.kapsulo.apps.movies.models.entity.Tv
import com.kapsulo.apps.movies.utils.*

@Database(entities = [(Movie::class), (Tv::class), (Person::class)], version = 3, exportSchema = false)
@TypeConverters(value = [
	(StringListConverter::class),
	(IntegerListConverter::class),
	(KeywordListConverter::class),
	(VideoListConverter::class),
	(ReviewListConverter::class)
])
abstract class AppDatabase : RoomDatabase() {
	abstract fun movieDao(): MovieDao
	abstract fun tvDao(): TvDao
	abstract fun peopleDao(): PeopleDao
}