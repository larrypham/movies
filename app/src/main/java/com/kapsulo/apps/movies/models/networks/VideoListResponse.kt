package com.kapsulo.apps.movies.models.networks

import com.google.gson.annotations.SerializedName
import com.kapsulo.apps.movies.models.NetworkResponseModel
import com.kapsulo.apps.movies.models.Video

data class VideoListResponse(
		@SerializedName("id")
		val id: Int,
		@SerializedName("results")
		val videos: List<Video>
) : NetworkResponseModel