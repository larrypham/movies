package com.kapsulo.apps.movies.view.ui.main

import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.GridLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.kapsulo.apps.movies.R
import com.kapsulo.apps.movies.extension.observeLiveData
import com.kapsulo.apps.movies.models.Resource
import com.kapsulo.apps.movies.models.Status
import com.kapsulo.apps.movies.models.entity.Movie
import com.kapsulo.apps.movies.view.adapter.MovieListAdapter
import com.kapsulo.apps.movies.view.ui.details.movie.MovieDetailActivity
import com.kapsulo.apps.movies.view.viewholder.MovieListViewHolder
import com.kapsulo.apps.movies.viewmodel.MainActivityViewModel
import com.skydoves.baserecyclerviewadapter.RecyclerViewPaginator
import dagger.android.support.AndroidSupportInjection
import kotlinx.android.synthetic.main.fragment_main_tv.*
import org.jetbrains.anko.startActivity
import org.jetbrains.anko.support.v4.toast
import javax.inject.Inject

class MovieListFragment: Fragment() , MovieListViewHolder.Delegate {
	@Inject lateinit var viewModelFactory: ViewModelProvider.Factory

	private lateinit var viewModel: MainActivityViewModel
	private val adapter = MovieListAdapter(this)

	private lateinit var paginator: RecyclerViewPaginator

	override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
		return inflater.inflate(R.layout.fragment_main_movie, container, false)
	}

	override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
		super.onViewCreated(view, savedInstanceState)
		initializeUI()
	}

	override fun onAttach(context: Context?) {
		AndroidSupportInjection.inject(this)
		super.onAttach(context)

		viewModel = ViewModelProviders.of(this, viewModelFactory).get(MainActivityViewModel::class.java)
		observeViewModel()
	}

	private fun initializeUI() {
		recycler_view.adapter = adapter
		recycler_view.layoutManager = GridLayoutManager(context, 2)
		paginator = RecyclerViewPaginator(recyclerView = recycler_view,
				isLoading = { viewModel.getMovieListValues()?.status == Status.LOADING },
				loadMore = { loadMore(it)},
				onLast = { viewModel.getMovieListValues()?.onLastPage!! })
		paginator.currentPage = 1
	}

	private fun observeViewModel() {
		observeLiveData(viewModel.getMovieListObservable()) { updateMovieList(it) }
		viewModel.postMoviePage(1)
	}

	private fun updateMovieList(resource: Resource<List<Movie>>) {
		when (resource.status) {
			Status.SUCCESS -> adapter.addMovieList(resource)
			Status.ERROR -> toast(resource.errorModel?.statusMessage.toString())
			Status.LOADING -> {}
		}
	}

	private fun loadMore(page: Int) {
		viewModel.postMoviePage(page)
	}

	override fun onItemClick(movie: Movie) {
		activity!!.startActivity<MovieDetailActivity>("movie" to movie)
	}
}
