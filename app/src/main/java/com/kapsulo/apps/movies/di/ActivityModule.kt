package com.kapsulo.apps.movies.di

import com.kapsulo.apps.movies.view.ui.details.movie.MovieDetailActivity
import com.kapsulo.apps.movies.view.ui.details.person.PersonDetailActivity
import com.kapsulo.apps.movies.view.ui.details.tv.TvDetailActivity
import com.kapsulo.apps.movies.view.ui.main.MainActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityModule {
	
	@ContributesAndroidInjector(modules = [MainActivityFragmentModule::class])
	abstract fun contributeMainActivity(): MainActivity

	@ContributesAndroidInjector
	abstract fun contributePersonDetailActivity(): PersonDetailActivity

	@ContributesAndroidInjector
	abstract fun contributeMovieDetailActivity(): MovieDetailActivity

	@ContributesAndroidInjector
	abstract fun contributeTvDetailActivity(): TvDetailActivity
}