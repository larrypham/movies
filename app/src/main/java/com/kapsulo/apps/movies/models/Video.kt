package com.kapsulo.apps.movies.models

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

data class Video(
		@SerializedName("id")
		val id: String?,
		
		@SerializedName("iso_639_1")
		val iso6391: String?,
		
		@SerializedName("iso_3166_1")
		val iso31661: String?,
		
		@SerializedName("key")
		val key: String?,
		
		@SerializedName("name")
		val name: String?,
		
		@SerializedName("site")
		val site: String?,
		
		@SerializedName("size")
		val size: Int,
		
		@SerializedName("type")
		val type: String?) : Parcelable {
	
	constructor(parcel: Parcel) : this(
			parcel.readString(),
			parcel.readString(),
			parcel.readString(),
			parcel.readString(),
			parcel.readString(),
			parcel.readString(),
			parcel.readInt(),
			parcel.readString())
	
	override fun writeToParcel(dest: Parcel?, flags: Int) = with(dest!!) {
		writeString(id)
		writeString(iso6391)
		writeString(iso31661)
		writeString(key)
		writeString(name)
		writeString(site)
		writeInt(size)
		writeString(type)
	}
	
	override fun describeContents(): Int = 0
	
	companion object CREATOR : Parcelable.Creator<Video> {
		override fun createFromParcel(parcel: Parcel): Video = Video(parcel)
		
		override fun newArray(size: Int): Array<Video?> = arrayOfNulls(size)
	}
}
