package com.kapsulo.apps.movies.mappers

import com.kapsulo.apps.movies.models.networks.PeopleResponse

class PeopleResponseMapper : NetworkResponseMapper<PeopleResponse> {
	override fun onLastPage(response: PeopleResponse): Boolean {
		return true
	}
}
