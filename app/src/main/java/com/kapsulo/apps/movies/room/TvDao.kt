package com.kapsulo.apps.movies.room

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.*
import com.kapsulo.apps.movies.models.entity.Tv

@Dao
interface TvDao {
	@Insert(onConflict = OnConflictStrategy.REPLACE)
	fun insert(tvs: List<Tv>)
	
	@Update
	fun update(tv: Tv)
	
	@Query("SELECT * FROM Tv WHERE id =:id_")
	fun getTv(id_: Int): Tv
	
	@Query("SELECT * FROM Tv WHERE page =:page_")
	fun getTvList(page_: Int): LiveData<List<Tv>>
}