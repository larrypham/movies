package com.kapsulo.apps.movies.models.networks

import com.kapsulo.apps.movies.models.Keyword
import com.kapsulo.apps.movies.models.NetworkResponseModel

data class KeywordListResponse(val id: Int, val keywords: List<Keyword>) : NetworkResponseModel