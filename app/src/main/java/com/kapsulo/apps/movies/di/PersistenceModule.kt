package com.kapsulo.apps.movies.di

import android.app.Application
import android.arch.persistence.room.Room
import android.support.annotation.NonNull
import com.kapsulo.apps.movies.room.AppDatabase
import com.kapsulo.apps.movies.room.MovieDao
import com.kapsulo.apps.movies.room.PeopleDao
import com.kapsulo.apps.movies.room.TvDao
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module(includes = [ViewModelModule::class])
class PersistenceModule {
	@Provides
	@Singleton
	fun provideDatabase(@NonNull application: Application): AppDatabase {
		return Room.databaseBuilder(application, AppDatabase::class.java, "TheMovies.db")
				.allowMainThreadQueries()
				.build()
	}
	
	@Provides
	@Singleton
	fun provideMovieDao(@NonNull database: AppDatabase): MovieDao {
		return database.movieDao()
	}
	
	@Provides
	@Singleton
	fun provideTvDao(@NonNull database: AppDatabase): TvDao {
		return database.tvDao()
	}
	
	@Provides
	@Singleton
	fun providePeopleDao(@NonNull database: AppDatabase): PeopleDao {
		return database.peopleDao()
	}
}