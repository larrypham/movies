package com.kapsulo.apps.movies.repository

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MediatorLiveData
import android.support.annotation.MainThread
import android.support.annotation.WorkerThread
import com.kapsulo.apps.movies.api.ApiResponse
import com.kapsulo.apps.movies.mappers.NetworkResponseMapper
import com.kapsulo.apps.movies.models.NetworkResponseModel
import com.kapsulo.apps.movies.models.Resource
import timber.log.Timber

abstract class NetworkBoundRepository<ResultType, RequestType : NetworkResponseModel, Mapper : NetworkResponseMapper<RequestType>>
internal constructor() {
	
	private val result: MediatorLiveData<Resource<ResultType>> = MediatorLiveData()
	
	init {
		Timber.d("Injection NetworkBoundRepository")
		
		val loadedFromDB = this.loadFromDB()
		result.addSource(loadedFromDB) { data ->
			result.removeSource(loadedFromDB)
			
			if (shouldFetch(data)) {
				result.postValue(Resource.loading(null))
				fetchFromNetwork(loadedFromDB)
			} else {
				result.addSource<ResultType>(loadedFromDB) { newData -> setValue(Resource.success(newData, false)) }
			}
		}
	}
	
	private fun fetchFromNetwork(loadedFromDB: LiveData<ResultType>) {
		val apiResponse = fetchService()
		result.addSource(apiResponse) { response ->
			response?.let {
				when (response.isSuccessful) {
					true -> {
						response.body?.let { requestType ->
							saveFetchData(requestType)
							val loaded = loadFromDB()
							
							result.addSource(loaded) { newData ->
								newData?.let { _ ->
									setValue(Resource.success(newData, mapper().onLastPage(response.body)))
								}
							}
						}
					}
					false -> {
						result.removeSource(loadedFromDB)
						onFetchFailed(response.message)
						response.message?.let { msg ->
							result.addSource<ResultType>(loadedFromDB) { newData ->
								setValue(Resource.error(msg, newData))
							}
						}
					}
				}
			}
		}
	}
	
	@MainThread
	private fun setValue(newValue: Resource<ResultType>) {
		result.value = newValue
	}
	
	fun asLiveData(): LiveData<Resource<ResultType>> {
		return result
	}
	
	@WorkerThread
	protected abstract fun saveFetchData(items: RequestType)
	
	@MainThread
	protected abstract fun shouldFetch(data: ResultType?): Boolean
	
	@MainThread
	protected abstract fun loadFromDB(): LiveData<ResultType>
	
	@MainThread
	protected abstract fun fetchService(): LiveData<ApiResponse<RequestType>>
	
	@MainThread
	protected abstract fun mapper(): Mapper
	
	@MainThread
	protected abstract fun onFetchFailed(message: String?)
}