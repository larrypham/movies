package com.kapsulo.apps.movies.models.entity

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import com.kapsulo.apps.movies.models.Keyword
import com.kapsulo.apps.movies.models.Review
import com.kapsulo.apps.movies.models.Video

@Entity(tableName = "Movie")
data class Movie(
		@SerializedName("page")
		var page: Int,
		var keywords: List<Keyword>? = ArrayList(),
		var videos: List<Video>? = ArrayList(),
		var reviews: List<Review>? = ArrayList(),
		
		@SerializedName("poster_path")
		val posterPath: String?,
		
		@SerializedName("adult")
		val adult: Boolean,
		
		@SerializedName("overview")
		val overview: String?,
		
		@SerializedName("release_date")
		val releaseDate: String?,
		
		@SerializedName("genre_ids")
		val genre_ids: List<Int>,
		
		@PrimaryKey
		@SerializedName("id")
		val id: Int,
		
		@SerializedName("original_title")
		val originalTitle: String?,
		
		@SerializedName("original_language")
		val originalLanguage: String?,
		
		@SerializedName("title")
		val title: String?,
		
		@SerializedName("backdrop_path")
		val backdropPath: String?,
		
		@SerializedName("popularity")
		val popularity: Float,
		
		@SerializedName("vote_count")
		val voteCount: Int,
		
		@SerializedName("video")
		val video: Boolean,
		
		val voteAverage: Float) : Parcelable {
	constructor(source: Parcel) : this(
			source.readInt(),
			ArrayList<Keyword>().apply { source.readList(this, Keyword::class.java.classLoader) },
			ArrayList<Video>().apply { source.readList(this, Video::class.java.classLoader) },
			ArrayList<Review>().apply { source.readList(this, Review::class.java.classLoader) },
			source.readString(),
			1 == source.readInt(),
			source.readString(),
			source.readString(),
			ArrayList<Int>().apply { source.readList(this, Int::class.java.classLoader) },
			source.readInt(),
			source.readString(),
			source.readString(),
			source.readString(),
			source.readString(),
			source.readFloat(),
			source.readInt(),
			1 == source.readInt(),
			source.readFloat()
	)
	
	override fun writeToParcel(dest: Parcel?, flags: Int) = with(dest!!) {
		writeInt(page)
		writeList(keywords)
		writeList(videos)
		writeList(reviews)
		writeString(posterPath)
		writeInt((if (adult) 1 else 0))
		writeString(overview)
		writeString(releaseDate)
		writeList(genre_ids)
		writeInt(id)
		writeString(originalTitle)
		writeString(originalLanguage)
		writeString(title)
		writeString(backdropPath)
		writeFloat(popularity)
		writeInt(voteCount)
		writeInt((if (video) 1 else 0))
		writeFloat(voteAverage)
	}
	
	override fun describeContents(): Int = 0
	
	companion object CREATOR : Parcelable.Creator<Movie> {
		override fun createFromParcel(parcel: Parcel): Movie {
			return Movie(parcel)
		}
		
		override fun newArray(size: Int): Array<Movie?> {
			return arrayOfNulls(size)
		}
	}
}