package com.kapsulo.apps.movies.view.ui.details.movie

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.Transformations
import android.arch.lifecycle.ViewModel
import com.kapsulo.apps.movies.models.Keyword
import com.kapsulo.apps.movies.models.Resource
import com.kapsulo.apps.movies.models.Review
import com.kapsulo.apps.movies.models.Video
import com.kapsulo.apps.movies.repository.MovieRepository
import com.kapsulo.apps.movies.utils.AbsentLiveData
import timber.log.Timber
import javax.inject.Inject

class MovieDetailViewModel @Inject constructor(private val repository: MovieRepository): ViewModel() {

	private val keywordIdLiveData: MutableLiveData<Int> = MutableLiveData()
	private val keywordsListLiveData: LiveData<Resource<List<Keyword>>>

	private val videoIdLiveData: MutableLiveData<Int> = MutableLiveData()
	private val videoListLiveData: LiveData<Resource<List<Video>>>

	private val reviewIdLiveData: MutableLiveData<Int> = MutableLiveData()
	private val reviewListLiveData: LiveData<Resource<List<Review>>>

	init {
		Timber.d("Injection MovieDetailViewModel")
		keywordsListLiveData = Transformations.switchMap(keywordIdLiveData) { _ ->
			keywordIdLiveData.value?.let { repository.loadKeywordList(it) } ?: AbsentLiveData.create()
		}

		videoListLiveData = Transformations.switchMap(videoIdLiveData) { _ ->
			videoIdLiveData.value?.let { repository.loadVideoList(it) } ?: AbsentLiveData.create()
		}

		reviewListLiveData = Transformations.switchMap(reviewIdLiveData) { _ ->
			reviewIdLiveData.value?.let { repository.loadReviewsList(it) } ?: AbsentLiveData.create()
		}
	}

	fun getKeywordListObservable() = keywordsListLiveData
	fun postKeywordId(id: Int) = keywordIdLiveData

	fun getVideoListObservable() = videoListLiveData
	fun postVideoId(id: Int) = videoIdLiveData.postValue(id)

	fun getReviewListObservable() = reviewListLiveData
	fun postReviewId(id: Int) = reviewIdLiveData.postValue(id)
}