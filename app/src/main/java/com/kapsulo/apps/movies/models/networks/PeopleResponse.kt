package com.kapsulo.apps.movies.models.networks

import com.google.gson.annotations.SerializedName
import com.kapsulo.apps.movies.models.NetworkResponseModel
import com.kapsulo.apps.movies.models.entity.Person

data class PeopleResponse(
		@SerializedName("page") val page: Int,
		@SerializedName("results") val results: List<Person>,
		@SerializedName("total_results") val totalResults: Int,
		@SerializedName("total_pages") val totalPages: Int) : NetworkResponseModel
