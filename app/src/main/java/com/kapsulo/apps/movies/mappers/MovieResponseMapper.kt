package com.kapsulo.apps.movies.mappers

import com.kapsulo.apps.movies.models.networks.DiscoverMovieResponse

class MovieResponseMapper : NetworkResponseMapper<DiscoverMovieResponse> {
	override fun onLastPage(response: DiscoverMovieResponse): Boolean {
		return true
	}
}
