package com.kapsulo.apps.movies.view.ui.main

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter

class PagerAdapter(fragmentManager: FragmentManager): FragmentPagerAdapter(fragmentManager) {
	override fun getItem(position: Int): Fragment {
		return when (position) {
			0 -> MovieListFragment()
			1 -> TvListFragment()
			else -> PersonListFragment()
		}
	}
	
	override fun getCount() = 3
}