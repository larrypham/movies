package com.kapsulo.apps.movies.view.adapter

import android.view.View
import com.kapsulo.apps.movies.R
import com.kapsulo.apps.movies.models.Resource
import com.kapsulo.apps.movies.models.entity.Person
import com.kapsulo.apps.movies.view.viewholder.PeopleViewHolder
import com.skydoves.baserecyclerviewadapter.BaseAdapter
import com.skydoves.baserecyclerviewadapter.BaseViewHolder
import com.skydoves.baserecyclerviewadapter.SectionRow

class PeopleAdapter(val delegate: PeopleViewHolder.Delegate): BaseAdapter() {

	init {
		addSection(ArrayList<Person>())
	}

	fun addPeople(resource: Resource<List<Person>>) {
		resource.data?.let {
			sections[0].addAll(resource.data)
			notifyDataSetChanged()
		}
	}

	override fun layout(sectionRow: SectionRow): Int {
		return R.layout.item_person
	}

	override fun viewHolder(layout: Int, view: View): BaseViewHolder {
		return PeopleViewHolder(view, delegate)
	}
}