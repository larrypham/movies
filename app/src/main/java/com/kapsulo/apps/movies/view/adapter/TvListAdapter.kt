package com.kapsulo.apps.movies.view.adapter

import android.view.View
import com.kapsulo.apps.movies.R
import com.kapsulo.apps.movies.models.Resource
import com.kapsulo.apps.movies.models.entity.Tv
import com.kapsulo.apps.movies.view.viewholder.TvListViewHolder
import com.skydoves.baserecyclerviewadapter.BaseAdapter
import com.skydoves.baserecyclerviewadapter.BaseViewHolder
import com.skydoves.baserecyclerviewadapter.SectionRow

class TvListAdapter(private val delegate: TvListViewHolder.Delegate): BaseAdapter() {

	init {
		addSection(ArrayList<Tv>())
	}

	fun addTvList(resource: Resource<List<Tv>>) {
		resource.data?.let {
			sections[0].addAll(it)
			notifyDataSetChanged()
		}
	}

	override fun layout(sectionRow: SectionRow): Int {
		return R.layout.item_poster
	}

	override fun viewHolder(layout: Int, view: View): BaseViewHolder {
		return TvListViewHolder(view, delegate)
	}
}