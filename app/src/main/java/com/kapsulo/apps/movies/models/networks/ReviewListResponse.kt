package com.kapsulo.apps.movies.models.networks

import com.google.gson.annotations.SerializedName
import com.kapsulo.apps.movies.models.NetworkResponseModel
import com.kapsulo.apps.movies.models.Review

data class ReviewListResponse(
		@SerializedName("id")
		val id: Int,
		
		@SerializedName("page")
		val page: Int,
		
		@SerializedName("results")
		val reviews: List<Review>,
		
		@SerializedName("total_pages")
		val totalPages: Int,
		
		@SerializedName("total_results")
		val totalResults: Int
) : NetworkResponseModel
