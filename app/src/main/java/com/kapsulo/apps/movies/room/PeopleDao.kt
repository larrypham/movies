package com.kapsulo.apps.movies.room

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.*
import com.kapsulo.apps.movies.models.entity.Person

@Dao
interface PeopleDao {
	
	@Insert(onConflict = OnConflictStrategy.REPLACE)
	fun insertPeople(people: List<Person>)
	
	@Update
	fun updatePerson(person: Person)
	
	@Query("SELECT * FROM People WHERE id =:id_")
	fun getPerson(id_: Int): Person
	
	@Query("SELECT * FROM People WHERE page =:page_")
	fun getPeople(page_: Int): LiveData<List<Person>>
}