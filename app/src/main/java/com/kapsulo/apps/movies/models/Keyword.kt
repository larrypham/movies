package com.kapsulo.apps.movies.models

import android.os.Parcel
import android.os.Parcelable

data class Keyword(val id: Int, val name: String) : Parcelable {
	
	constructor(parcel: Parcel) : this(
			parcel.readInt(),
			parcel.readString()!!) {
	}
	
	override fun writeToParcel(dest: Parcel?, flags: Int) = with(dest!!) {
		writeInt(id)
		writeString(name)
	}
	
	override fun describeContents(): Int = 0
	
	companion object CREATOR : Parcelable.Creator<Keyword> {
		override fun createFromParcel(source: Parcel): Keyword = Keyword(source)
		
		override fun newArray(size: Int): Array<Keyword?> = arrayOfNulls(size)
	}
}