package com.kapsulo.apps.movies.view.adapter

import android.view.View
import com.kapsulo.apps.movies.R
import com.kapsulo.apps.movies.models.Resource
import com.kapsulo.apps.movies.models.Video
import com.kapsulo.apps.movies.view.viewholder.VideoListViewHolder
import com.skydoves.baserecyclerviewadapter.BaseAdapter
import com.skydoves.baserecyclerviewadapter.BaseViewHolder
import com.skydoves.baserecyclerviewadapter.SectionRow

class VideoListAdapter(private val delegate: VideoListViewHolder.Delegate): BaseAdapter() {

	init {
		addSection(ArrayList<Video>())
	}

	fun addVideoList(resource: Resource<List<Video>>) {
		resource.data?.let {
			sections[0].addAll(it)
			notifyDataSetChanged()
		}
	}

	override fun layout(sectionRow: SectionRow): Int {
		return R.layout.item_video
	}

	override fun viewHolder(layout: Int, view: View): BaseViewHolder {
		return VideoListViewHolder(view, delegate)
	}
}