package com.kapsulo.apps.movies.models

data class ResponseModel(val page: Int, val results: Any, val totalResults: Int, val totalPages: Int)