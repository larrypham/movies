package com.kapsulo.apps.movies.models

import com.google.gson.Gson
import com.google.gson.JsonSyntaxException
import com.kapsulo.apps.movies.models.networks.ErrorModel

/**
 * A generic class that holds a value with its loading status.
 */
class Resource<out T>(val status: Status, val data: T?, val message: String?, val onLastPage: Boolean) {
	
	var errorModel: ErrorModel? = null
	
	init {
		message?.let {
			errorModel = try {
				val gson = Gson()
				gson.fromJson(message, ErrorModel::class.java) as ErrorModel
			} catch (e: JsonSyntaxException) {
				ErrorModel(400, message, false)
			}
		}
	}
	
	override fun equals(other: Any?): Boolean {
		if (this === other) return true
		if (javaClass != other?.javaClass) return false
		
		other as Resource<*>
		
		if (status != other.status) return false
		if (data != other.data) return false
		if (message != other.message) return false
		if (onLastPage != other.onLastPage) return false
		if (errorModel != other.errorModel) return false
		
		return true
	}
	
	override fun hashCode(): Int {
		var result = status.hashCode()
		result = 31 * result + (data?.hashCode() ?: 0)
		result = 31 * result + (message?.hashCode() ?: 0)
		result = 31 * result + onLastPage.hashCode()
		result = 31 * result + (errorModel?.hashCode() ?: 0)
		return result
	}
	
	override fun toString(): String {
		return "Resource(status=$status, data=$data, message=$message, onLastPage=$onLastPage, errorModel=$errorModel)"
	}
	
	companion object {
		fun <T> success(data: T?, onLastPage: Boolean): Resource<T> {
			return Resource(status = Status.SUCCESS, data = data, message = null, onLastPage = onLastPage)
		}
		
		fun <T> error(msg: String, data: T?): Resource<T> {
			return Resource(status = Status.ERROR, data = data, message = msg, onLastPage = true)
		}
		
		fun <T> loading(data: T?): Resource<T> {
			return Resource(status = Status.LOADING, data = data, message = null, onLastPage = false)
		}
	}
}
