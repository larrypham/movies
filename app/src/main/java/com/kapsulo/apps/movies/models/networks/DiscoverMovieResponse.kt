package com.kapsulo.apps.movies.models.networks

import com.google.gson.annotations.SerializedName
import com.kapsulo.apps.movies.models.NetworkResponseModel
import com.kapsulo.apps.movies.models.entity.Movie

data class DiscoverMovieResponse(
		@SerializedName("page")
		val page: Int,
		
		@SerializedName("results")
		val results: List<Movie>,
		
		@SerializedName("total_results")
		val totalResults: Int,
		
		@SerializedName("total_pages")
		val totalPages: Int
) : NetworkResponseModel