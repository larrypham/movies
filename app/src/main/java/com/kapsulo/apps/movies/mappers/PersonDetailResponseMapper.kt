package com.kapsulo.apps.movies.mappers

import com.kapsulo.apps.movies.models.networks.PersonDetail

class PersonDetailResponseMapper : NetworkResponseMapper<PersonDetail> {
	override fun onLastPage(response: PersonDetail): Boolean {
		return true
	}
}
