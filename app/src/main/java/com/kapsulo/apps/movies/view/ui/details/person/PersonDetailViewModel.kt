package com.kapsulo.apps.movies.view.ui.details.person

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.Transformations
import android.arch.lifecycle.ViewModel
import com.kapsulo.apps.movies.models.Resource
import com.kapsulo.apps.movies.models.networks.PersonDetail
import com.kapsulo.apps.movies.repository.PeopleRepository
import com.kapsulo.apps.movies.utils.AbsentLiveData
import timber.log.Timber
import javax.inject.Inject

class PersonDetailViewModel @Inject constructor(private val repository: PeopleRepository): ViewModel() {
	private val personIdLiveData: MutableLiveData<Int> = MutableLiveData()
	private val personLiveData: LiveData<Resource<PersonDetail>>

	init {
		Timber.d("Injection: PersonalDetailViewModel")
		personLiveData = Transformations.switchMap(personIdLiveData) { it ->
			personIdLiveData.value?.let { repository.loadPersonDetail(it) } ?: AbsentLiveData.create()
		}
	}

	fun getPersonObservable() = personLiveData

	fun postPersonId(id: Int) = personIdLiveData.postValue(id)
}