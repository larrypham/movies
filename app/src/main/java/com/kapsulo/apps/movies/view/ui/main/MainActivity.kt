package com.kapsulo.apps.movies.view.ui.main

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import com.kapsulo.apps.movies.R
import com.kapsulo.apps.movies.utils.MainNavigationUtils
import dagger.android.AndroidInjection
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.HasSupportFragmentInjector
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject

class MainActivity : AppCompatActivity(), HasSupportFragmentInjector {
	
	@Inject
	lateinit var fragmentInjector: DispatchingAndroidInjector<Fragment>
	
	override fun onCreate(savedInstanceState: Bundle?) {
		AndroidInjection.inject(this)
		super.onCreate(savedInstanceState)
		setContentView(R.layout.activity_main)
		
		initializeUI()
	}
	
	private fun initializeUI() {
		main_viewpager.adapter = PagerAdapter(supportFragmentManager)
		main_viewpager.offscreenPageLimit = 3
		MainNavigationUtils.setComponents(this, main_viewpager, main_bottom_nav)
	}
	
	override fun supportFragmentInjector(): AndroidInjector<Fragment> {
		return fragmentInjector
	}
}
