package com.kapsulo.apps.movies.view.ui.details.person

import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.support.v4.app.ActivityOptionsCompat
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentActivity
import android.support.v4.view.ViewCompat
import android.support.v7.app.AppCompatActivity
import android.view.View
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.Target
import com.kapsulo.apps.movies.R
import com.kapsulo.apps.movies.api.Api
import com.kapsulo.apps.movies.extension.checkIsMaterialVersion
import com.kapsulo.apps.movies.extension.observeLiveData
import com.kapsulo.apps.movies.models.Resource
import com.kapsulo.apps.movies.models.Status
import com.kapsulo.apps.movies.models.entity.Person
import com.kapsulo.apps.movies.models.networks.PersonDetail
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.activity_person_detail.*
import kotlinx.android.synthetic.main.toolbar_default.*
import org.jetbrains.anko.startActivityForResult
import org.jetbrains.anko.toast
import javax.inject.Inject

class PersonDetailActivity: AppCompatActivity() {

	@Inject
	lateinit var viewModelFactory: ViewModelProvider.Factory

	private val viewModel by lazy { ViewModelProviders.of(this, viewModelFactory).get(PersonDetailViewModel::class.java) }

	override fun onCreate(savedInstanceState: Bundle?) {
		AndroidInjection.inject(this)
		super.onCreate(savedInstanceState)

		setContentView(R.layout.activity_person_detail)
		supportPostponeEnterTransition()
		initializeUI()
	}

	private fun initializeUI() {
		toolbar_home.setOnClickListener { onBackPressed() }
		toolbar_title.text = getPersonFromIntent().name
		getPersonFromIntent().profilePath?.let {
			Glide.with(this).load(Api.getPosterPath(it))
					.apply(RequestOptions().circleCrop())
					.listener(object: RequestListener<Drawable> {
						override fun onLoadFailed(e: GlideException?, model: Any?, target: Target<Drawable>?, isFirstResource: Boolean): Boolean {
							supportStartPostponedEnterTransition()
							observeViewModel()
							return false
						}

						override fun onResourceReady(resource: Drawable?, model: Any?, target: Target<Drawable>?, dataSource: DataSource?, isFirstResource: Boolean): Boolean {
							supportStartPostponedEnterTransition()
							observeViewModel()
							return false
						}
					})
					.into(iv_person_detail_profile)
		}
	}

	private fun observeViewModel() {
		observeLiveData(viewModel.getPersonObservable()) { updatePersonDetail(it) }
		viewModel.postPersonId(getPersonFromIntent().id)
	}

	private fun updatePersonDetail(resource: Resource<PersonDetail>) {
		when (resource.status) {
			Status.SUCCESS -> {
				resource.data?.let {
					tv_person_detail_biography.text = it.biography
					detail_person_tags.tags = it.alsoKnownAs

					if (it.alsoKnownAs!!.isNotEmpty()) {
						detail_person_tags.visibility = View.VISIBLE
					}
				}
			}
			Status.ERROR -> toast(resource.errorModel?.statusMessage.toString())
			Status.LOADING -> {}
		}
	}

	private fun getPersonFromIntent(): Person {
		return intent.getParcelableExtra("person") as Person
	}

	companion object {
		const val intent_request_code = 1000

		fun startActivity(fragment: Fragment, activity: FragmentActivity, person: Person, view: View) {
			if (activity.checkIsMaterialVersion()) {
				val intent = Intent(activity, PersonDetailActivity::class.java)
				val options = ActivityOptionsCompat.makeSceneTransitionAnimation(activity, view, ViewCompat.getTransitionName(view)!!)
				intent.putExtra("person", person)
				activity.startActivityFromFragment(fragment, intent, intent_request_code, options.toBundle())
			} else {
				activity.startActivityForResult<PersonDetailActivity>(intent_request_code, "person" to Person)
			}
		}
	}
}