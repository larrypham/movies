package com.kapsulo.apps.movies.mappers

import com.kapsulo.apps.movies.models.networks.VideoListResponse

class VideoResponseMapper : NetworkResponseMapper<VideoListResponse> {
	override fun onLastPage(response: VideoListResponse): Boolean {
		return false
	}
}
