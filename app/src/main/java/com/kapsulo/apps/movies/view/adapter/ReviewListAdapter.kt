package com.kapsulo.apps.movies.view.adapter

import android.view.View
import com.kapsulo.apps.movies.R
import com.kapsulo.apps.movies.models.Resource
import com.kapsulo.apps.movies.models.Review
import com.kapsulo.apps.movies.view.viewholder.ReviewListViewHolder
import com.skydoves.baserecyclerviewadapter.BaseAdapter
import com.skydoves.baserecyclerviewadapter.BaseViewHolder
import com.skydoves.baserecyclerviewadapter.SectionRow

class ReviewListAdapter: BaseAdapter() {

	init {
		addSection(ArrayList<Review>())
	}

	fun addReviewList(resource: Resource<List<Review>>) {
		resource.data?.let {
			sections[0].addAll(it)
		}
		notifyDataSetChanged()
	}

	override fun layout(sectionRow: SectionRow): Int {
		return R.layout.item_review
	}

	override fun viewHolder(layout: Int, view: View): BaseViewHolder {
		return ReviewListViewHolder(view)
	}
}