package com.kapsulo.apps.movies.mappers

import com.kapsulo.apps.movies.models.networks.VideoListResponse

class VideoListResponseMapper : NetworkResponseMapper<VideoListResponse> {
	override fun onLastPage(response: VideoListResponse): Boolean {
		return true
	}
}
