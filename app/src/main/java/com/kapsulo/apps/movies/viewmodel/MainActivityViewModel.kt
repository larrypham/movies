package com.kapsulo.apps.movies.viewmodel

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.Transformations
import android.arch.lifecycle.ViewModel
import com.kapsulo.apps.movies.models.Resource
import com.kapsulo.apps.movies.models.entity.Movie
import com.kapsulo.apps.movies.models.entity.Person
import com.kapsulo.apps.movies.models.entity.Tv
import com.kapsulo.apps.movies.repository.DiscoverRepository
import com.kapsulo.apps.movies.repository.PeopleRepository
import com.kapsulo.apps.movies.utils.AbsentLiveData
import timber.log.Timber
import javax.inject.Inject

class MainActivityViewModel @Inject
constructor(val discoverRepository: DiscoverRepository, val peopleRepository: PeopleRepository) : ViewModel() {
	
	private var moviePageLiveData: MutableLiveData<Int> = MutableLiveData()
	private val movieListLiveData: LiveData<Resource<List<Movie>>>
	
	private var tvPageLiveData: MutableLiveData<Int> = MutableLiveData()
	private var tvListLiveData: LiveData<Resource<List<Tv>>>
	
	private var peoplePageLiveData: MutableLiveData<Int> = MutableLiveData()
	private var peopleLiveData: LiveData<Resource<List<Person>>>
	
	init {
		Timber.d("injection MainActivityViewModel")
		
		movieListLiveData = Transformations.switchMap(moviePageLiveData) { page ->
			moviePageLiveData.value?.let { discoverRepository.loadMovies(page) } ?: AbsentLiveData.create()
		}
		
		tvListLiveData = Transformations.switchMap(tvPageLiveData) { page ->
			tvPageLiveData.value?.let { discoverRepository.loadTvs(page) }
		}
		
		peopleLiveData = Transformations.switchMap(peoplePageLiveData) { page ->
			peoplePageLiveData.value?.let { peopleRepository.loadPeople(page) }
		}
	}
	
	fun getMovieListObservable() = movieListLiveData
	
	fun postMoviePage(page: Int) = moviePageLiveData.postValue(page)
	
	fun getMovieListValues() = getMovieListObservable().value

	fun getTvListObservable() = tvListLiveData

	fun getTvListValues() = getTvListObservable().value

	fun postTvPage(page: Int) = tvPageLiveData.postValue(page)

	fun getPeopleObservable() = peopleLiveData

	fun getPeopleValues() = getPeopleObservable().value

	fun postPeoplePage(page: Int) = peoplePageLiveData.postValue(page)
}
