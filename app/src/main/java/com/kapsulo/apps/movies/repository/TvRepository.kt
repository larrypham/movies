package com.kapsulo.apps.movies.repository

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import com.kapsulo.apps.movies.api.ApiResponse
import com.kapsulo.apps.movies.api.TvService
import com.kapsulo.apps.movies.mappers.KeywordResponseMapper
import com.kapsulo.apps.movies.mappers.ReviewsResponseMapper
import com.kapsulo.apps.movies.mappers.VideoResponseMapper
import com.kapsulo.apps.movies.models.Keyword
import com.kapsulo.apps.movies.models.Resource
import com.kapsulo.apps.movies.models.Review
import com.kapsulo.apps.movies.models.Video
import com.kapsulo.apps.movies.models.networks.KeywordListResponse
import com.kapsulo.apps.movies.models.networks.ReviewListResponse
import com.kapsulo.apps.movies.models.networks.VideoListResponse
import com.kapsulo.apps.movies.room.TvDao
import timber.log.Timber
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class TvRepository @Inject
constructor(val tvService: TvService, val tvDao: TvDao): Repository {
	
	fun loadKeywordList(id: Int): LiveData<Resource<List<Keyword>>> {
		return object : NetworkBoundRepository<List<Keyword>, KeywordListResponse, KeywordResponseMapper>() {
			override fun saveFetchData(items: KeywordListResponse) {
				val tv = tvDao.getTv(id_ = id)
				tv.keywords = items.keywords
				tvDao.update(tv = tv)
			}
			
			override fun shouldFetch(data: List<Keyword>?): Boolean {
				return data == null || data.isEmpty()
			}
			
			override fun loadFromDB(): LiveData<List<Keyword>> {
				val movie = tvDao.getTv(id_ = id)
				val data: MutableLiveData<List<Keyword>> = MutableLiveData()
				data.value = movie.keywords
				return data
			}
			
			override fun fetchService(): LiveData<ApiResponse<KeywordListResponse>> {
				return tvService.fetchKeywords(id = id)
			}
			
			override fun mapper(): KeywordResponseMapper {
				return KeywordResponseMapper()
			}
			
			override fun onFetchFailed(message: String?) {
				Timber.d("onFetchFailed: $message")
			}
			
		}.asLiveData()
	}
	
	fun loadVideoList(id: Int): LiveData<Resource<List<Video>>> {
		return object : NetworkBoundRepository<List<Video>, VideoListResponse, VideoResponseMapper>() {
			override fun saveFetchData(items: VideoListResponse) {
				val tv = tvDao.getTv(id_ = id)
				tv.videos = items.videos
				tvDao.update(tv = tv)
			}
			
			override fun shouldFetch(data: List<Video>?): Boolean {
				return data == null || data.isEmpty()
			}
			
			override fun loadFromDB(): LiveData<List<Video>> {
				val movie = tvDao.getTv(id_ = id)
				val data: MutableLiveData<List<Video>> = MutableLiveData()
				data.value = movie.videos
				return data
			}
			
			override fun fetchService(): LiveData<ApiResponse<VideoListResponse>> {
				return tvService.fetchVideos(id = id)
			}
			
			override fun mapper(): VideoResponseMapper {
				return VideoResponseMapper()
			}
			
			override fun onFetchFailed(message: String?) {
				Timber.d("onFetchFailed: $message")
			}
			
		}.asLiveData()
	}
	
	fun loadReviewsList(id: Int): LiveData<Resource<List<Review>>> {
		return object : NetworkBoundRepository<List<Review>, ReviewListResponse, ReviewsResponseMapper>() {
			override fun saveFetchData(items: ReviewListResponse) {
				val tv = tvDao.getTv(id_ = id)
				tv.reviews = items.reviews
				tvDao.update(tv = tv)
			}
			
			override fun shouldFetch(data: List<Review>?): Boolean {
				return data == null || data.isEmpty()
			}
			
			override fun loadFromDB(): LiveData<List<Review>> {
				val movie = tvDao.getTv(id_ = id)
				val data: MutableLiveData<List<Review>> = MutableLiveData()
				data.value = movie.reviews
				return data
			}
			
			override fun fetchService(): LiveData<ApiResponse<ReviewListResponse>> {
				return tvService.fetchReviews(id = id)
			}
			
			override fun mapper(): ReviewsResponseMapper {
				return ReviewsResponseMapper()
			}
			
			override fun onFetchFailed(message: String?) {
				Timber.d("onFetchFailed: $message")
			}
			
		}.asLiveData()
	}
	
}
