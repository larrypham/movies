package com.kapsulo.apps.movies.mappers

import com.kapsulo.apps.movies.models.networks.KeywordListResponse

class KeywordResponseMapper : NetworkResponseMapper<KeywordListResponse> {
	override fun onLastPage(response: KeywordListResponse): Boolean {
		return true
	}
}