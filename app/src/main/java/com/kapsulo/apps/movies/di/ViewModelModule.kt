package com.kapsulo.apps.movies.di

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import com.kapsulo.apps.movies.view.ui.details.movie.MovieDetailViewModel
import com.kapsulo.apps.movies.view.ui.details.person.PersonDetailViewModel
import com.kapsulo.apps.movies.view.ui.details.tv.TvDetailViewModel
import com.kapsulo.apps.movies.viewmodel.AppViewModelFactory
import com.kapsulo.apps.movies.viewmodel.MainActivityViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Suppress("unused")
@Module
internal abstract class ViewModelModule {
	
	@Binds
	@IntoMap
	@ViewModelKey(MainActivityViewModel::class)
	internal abstract fun bindMainActivityViewModels(mainActivityViewModel: MainActivityViewModel): ViewModel

	@Binds
	@IntoMap
	@ViewModelKey(PersonDetailViewModel::class)
	internal abstract fun bindPersonDetailViewModel(personDetailViewModel: PersonDetailViewModel): ViewModel

	@Binds
	@IntoMap
	@ViewModelKey(MovieDetailViewModel::class)
	internal abstract fun bindMovieDetailViewModel(movieDetailViewModel: MovieDetailViewModel): ViewModel

	@Binds
	@IntoMap
	@ViewModelKey(TvDetailViewModel::class)
	internal abstract fun bindTvDetailViewModel(tvDetailViewModel: TvDetailViewModel): ViewModel

	@Binds
	internal abstract fun bindViewModelFactory(factory: AppViewModelFactory): ViewModelProvider.Factory
}